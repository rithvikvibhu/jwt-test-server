const express = require('express')
const bodyParser = require('body-parser')
const path = require('path');
const jwt = require('jsonwebtoken')

const app = express()
const port = process.env.PORT || 3000

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.use('/sso', require('./sso.js'));
app.use('/jwt', require('./jwt.js'));
app.use('/', require('./jwt.js'));

// app.use(express.static('public'))
app.get('/', (req, res) => res.send('Hello World!'))

// 1) Pesu IO Login page displayed
app.get('/io-side/pesuiologinpage.html', (req, res) => res.sendFile(path.join(__dirname + '/public/pesuiologinpage.html')))

// 3) Serve PesuAcademy login page
app.get('/academy-side/pesuacademyloginpage.html', (req, res) => res.sendFile(path.join(__dirname + '/public/pesuacademyloginpage.html')))


app.listen(port, () => console.log(`listening on port ${port}!`))
