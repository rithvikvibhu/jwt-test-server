var express = require('express');
var router = express.Router();
var crypto = require('crypto');
var querystring = require('querystring');

const SECRET = process.env.JWT_SECRET || 'SUPER-SECRET-SECRET'

router.get('/', function(req, res, next) {
  res.send({ message: 'sso auth' });
});

// 2b) Login with PesuAcademy button sent here. Generate a random signed token, send to PesuAcademy login page
router.get('/io-side/pesuacademy/redirect', (req, res) => {
  var nonce = crypto.randomBytes(16).toString('base64');
  let {sso, sig} = encodePayload({nonce: nonce, callback: '/sso/io-side/pesuacademy/callback'});
  res.redirect(`/academy-side/pesuacademyloginpage.html?sso=${sso}&sig=${sig}`);
})

// 4) PesuAcademy verifies login and generates new signed token
router.post('/academy-side/login', (req, res) => {
  if (req.body.username != 'a' || req.body.password != 'a') {
    res.send('Invalid cred')
    return;
  }
  try {
    if (!verifySig(req.body.sso, req.body.sig)) {
      res.send('Invalid signature')
      return;
    }
    let {nonce, callback} = decodePayload(req.body.sso);
    let {sso, sig} = encodePayload({nonce: nonce, email: 'a@a.aa', external_id: req.body.username, username: req.body.username, name: req.body.username});
    res.redirect(`${callback}?sso=${sso}&sig=${sig}`)
  } catch (e) {
    console.log(e)
    res.send(e.message)
  }
})

// 5) Pesuio verifies token from pesuacademy and logs in user
router.get('/io-side/pesuacademy/callback', (req, res) => {
  try {
    if (!verifySig(req.query.sso, req.query.sig)) {
      res.send('Invalid signature')
      return;
    }
    var payload = decodePayload(req.query.sso);
    res.send({message: 'Login by PesuAcademy (SSO) successful', data: payload})
  } catch (e) {
    console.log(e)
    res.send(e.message)
  }
})

function encodePayload(payload) {
  var urlencodedPayload = querystring.stringify(payload);
  var sso = Buffer.from(urlencodedPayload).toString('base64');
  var sig = crypto.createHmac('sha256', SECRET).update(sso).digest('hex');
  return {sso, sig}
}

function decodePayload(payload) {
  var b64decodedPayload = Buffer.from(payload, 'base64').toString('utf8');
  var urldecodedPayload = querystring.parse(b64decodedPayload);
  return urldecodedPayload;
}

function verifySig(sso, sig) {
  var computedSig = crypto.createHmac('sha256', SECRET).update(sso).digest('hex');
  return computedSig == sig
}

module.exports = router;
