var express = require('express');
var router = express.Router();
var jwt = require('jsonwebtoken');

const SECRET = process.env.JWT_SECRET || 'SUPER-SECRET-SECRET'

// 2a) Login form (username+password) sent here. Flow ends here.
router.post('/io-side/login', (req, res) => {
  if (req.body.username != 'a' || req.body.password != 'a') {
    res.send('Invalid cred')
    return;
  }
  res.send('Login by username+password successful')
})

// 2b) Login with PesuAcademy button sent here. Generate a random signed token, send to PesuAcademy login page
router.get('/io-side/pesuacademy/redirect', (req, res) => {
  var token = jwt.sign({ callback: '/jwt/io-side/pesuacademy/callback' }, SECRET, {expiresIn: '10m', audience: 'pesuacademy', issuer: 'pesuio'})
  console.log('generated token:', token);
  res.redirect(`/academy-side/pesuacademyloginpage.html?token=${token}`)
})

// 4) PesuAcademy verifies login and generates new signed token
router.post('/academy-side/login', (req, res) => {
  if (req.body.username != 'a' || req.body.password != 'a') {
    res.send('Invalid cred')
    return;
  }
  try {
    var decoded = jwt.verify(req.body.token, SECRET)
    console.log('received token data from pesuio:', decoded)
    var newToken = jwt.sign({ username: req.body.username }, SECRET, {expiresIn: '10m', audience: 'pesuio', issuer: 'pesuacademy'})
    res.redirect(`${decoded.callback}?token=${newToken}`)
  } catch (e) {
    console.log(e)
    res.send(e.message)
  }
})

// 5) Pesuio verifies token from pesuacademy and logs in user
router.get('/io-side/pesuacademy/callback', (req, res) => {
  try {
    var decoded = jwt.verify(req.query.token, SECRET)
    console.log('received token data from pesuacademy:', decoded)
    res.send({message: 'Login by PesuAcademy (JWT) successful', data: decoded})
  } catch (e) {
    console.log(e)
    res.send(e.message)
  }
})

module.exports = router;
